function output = sum(this)
% Sum all values
% Copyright (c) 2014 Alex Henderson (alex.henderson@manchester.ac.uk)

    output = sum(this.data);
end
